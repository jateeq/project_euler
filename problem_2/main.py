import time

def isEven(target):
    return target % 2 == 0

def basic_solution(upper_bound):
    prev_number = 1
    current_number = 2
    next_number = prev_number + current_number

    total = current_number

    while True:
        next_number = prev_number + current_number
        if not next_number < upper_bound:
            break

        prev_number = current_number
        current_number = next_number
        if isEven(current_number):
            total += current_number

    return total

def main():
    UPPER_BOUND = 4e6
    t0 = time.time()
    total = basic_solution(UPPER_BOUND)
    t1 = time.time()
    print "Answer from basic solution: " + str(total)
    print "Runtime: " + str(t1-t0)

if __name__ == "__main__":
    main()
