import pytest
from main import *

def test_isEven():
    assert isEven(2) == True
    assert isEven(3) == False

def test_basic_solution():
    assert basic_solution(10) == 10
    assert basic_solution(100) == 44
