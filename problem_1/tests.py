import pytest
from main import *

def test_isMultiple():
    assert isMultiple(4,3) == False
    assert isMultiple(6,3) == True

def test_total_multiples_of_3_or_5():
    assert total_multiples_of_3_or_5(10) == 23
