def isMultiple(target, divisor):
    return target % divisor == 0

def total_multiples_of_3_or_5(upper_bound):
    total = 0
    for i in range(upper_bound):
        if isMultiple(i, 3) or isMultiple(i, 5):
            total += i
    return total

def main():
    UPPER_BOUND= 1000
    total = total_multiples_of_3_or_5(UPPER_BOUND)

    print "Answer: " + str(total)


if __name__ == "__main__":
    main()



